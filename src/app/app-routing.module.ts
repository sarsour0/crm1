import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientComponent } from './client/client.component';
import { EventComponent } from './event/event.component';
import { HomeComponent } from './home/home.component';
import { ProspectComponent } from './prospect/prospect.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'event',component:EventComponent},
  {path:'client',component:ClientComponent},
  {path:'prospect',component:ProspectComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
