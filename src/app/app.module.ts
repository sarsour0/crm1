import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AsidebarComponent } from './asidebar/asidebar.component';
import { DashbordComponent } from './dashbord/dashbord.component';
import { HomeComponent } from './home/home.component';
import { RightbarComponent } from './rightbar/rightbar.component';
import { EventComponent } from './event/event.component';
import { DialogEventComponent } from './dialog-event/dialog-event.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';


import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core'
import {MatRadioModule} from '@angular/material/radio';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import { ApiService } from './services/api.service';
import { ClientComponent } from './client/client.component';
import { DialogClientComponent } from './dialog-client/dialog-client.component';
import { CommonModule } from '@angular/common';
import { ProspectComponent } from './prospect/prospect.component';
import { DialogProspectComponent } from './dialog-prospect/dialog-prospect.component';
import { DetailsClientComponent } from './details-client/details-client.component';



@NgModule({
  declarations: [
    AppComponent,
    AsidebarComponent,
    DashbordComponent,
    HomeComponent,
    RightbarComponent,
    EventComponent,



    DialogEventComponent,
       ClientComponent,
       DialogClientComponent,
       ProspectComponent,
       DialogProspectComponent,
       DetailsClientComponent
      

  ],

  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,

    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    


  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
