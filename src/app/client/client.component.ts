import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DetailsClientComponent } from '../details-client/details-client.component';
import { DialogClientComponent } from '../dialog-client/dialog-client.component';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  displayedColumns: string[] = ['clientId','nom', 'prenom','email' ,'cin','lieu','action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!:MatPaginator;
  @ViewChild(MatSort) sort!:MatSort;

constructor(private dialog : MatDialog,private api :ApiService){


}
darkmode=false;
  modetoggle(){
   this.darkmode =!this.darkmode;
   document.documentElement.setAttribute('data-theme',this.darkmode ? "dark" : "light");
 }
ngOnInit(): void {
    this.api.getClient().subscribe((res)=>{
    this.dataSource= new MatTableDataSource(res);
    this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      });
}
openDialog() {
  this.dialog.open(DialogClientComponent , {
   width:'30%'
  });
}
openDetails(row:any) {
  this.dialog.open(DetailsClientComponent , {
   width:'30%',
   data:row
  })
}
getClient(){
  this.api.getClient()
  .subscribe({
    next:(res)=>{

     console.log( this.dataSource= new MatTableDataSource(res["http://localhost:8080/api/events"]));
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    },
    error:(err)=>{

      alert("Error while fetching the Records !!")
    }

  })
}

editProduct(row : any){

  this.dialog.open(DialogClientComponent,{
    width :'30%',
    data:row
  }).afterClosed().subscribe(val=>{
    if(val==='update'){
      this.getClient();
    }
  })


}
deleteClient(clientId:number){
  this.api.deleteClient(clientId)
  .subscribe({
    next:(res:any)=>{
      alert("Product Deleted Successfully");
      this.getClient();
      window.location.reload();
    },
    error:()=>{
      alert("Error while deleting the product!! ")
    }
  })
}
applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}

}
