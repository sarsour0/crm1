import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-details-client',
  templateUrl: './details-client.component.html',
  styleUrls: ['./details-client.component.css']
})
export class DetailsClientComponent implements OnInit {
  clients: any[] = [];
  constructor(private api : ApiService,
    @Inject(MAT_DIALOG_DATA) public Details : any,
    private dialogRef :MatDialogRef<DetailsClientComponent>) { }

  ngOnInit(): void {
    this.api.getClient().subscribe((res:any[])=>{
     console.log( this.clients=res);
  
        });
  }

}
