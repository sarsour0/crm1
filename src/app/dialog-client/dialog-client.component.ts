import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-dialog-client',
  templateUrl: './dialog-client.component.html',
  styleUrls: ['./dialog-client.component.css']
})
export class DialogClientComponent implements OnInit {

  productForm !: FormGroup;
  actionBtn: string="save"
  constructor(private fromBuilder : FormBuilder,
    private api: ApiService ,
    @Inject(MAT_DIALOG_DATA) public editData : any,
    private dialogRef :MatDialogRef<DialogClientComponent > ) { }

  ngOnInit(): void {
    this.productForm=this.fromBuilder.group({

      nom :['',Validators.required],
      prenom :['',Validators.required],
      cin :['',Validators.required],
      email :['',Validators.required],
      lieu:['',Validators.required],
      

    })
    if(this.editData){
      this.actionBtn="Update";

      this.productForm.controls['nom' ].setValue(this.editData.nom);

      this.productForm.controls['prenom'].setValue(this.editData.prenom);
      this.productForm.controls['cin']. setValue(this.editData.cin);
      this.productForm.controls['email'].setValue(this.editData.email);
      this.productForm.controls['lieu'].setValue(this.editData.lieu);
     

     }

  }
  addProduct(){
    console.log(this.productForm.value);
    if(!this.editData){
      if(this.productForm.valid){
        this.api.postProduct(this.productForm.value)
        .subscribe({
          next:(res)=>{
            alert("Product added successfully")
            this.productForm.reset();
            this.dialogRef.close('save');
            window.location.reload();
          },
          error:()=>{
            alert("Error while adding the product")
          }
        })
      }

    }else {
      this.updateProduct()
    }
  }
  updateProduct(){
    this.api.putClient( this.editData.clientId,this.productForm.value)
    
     .subscribe({
       next:(res)=>{
         alert("Product updated Successfully");
         this.productForm.reset();
         this.dialogRef.close('update');
         window.location.reload();
       },
       error:()=>{
         alert("Error while updating the record!!");
       }
     })
   }

}
