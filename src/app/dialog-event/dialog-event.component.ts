import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-dialog-event',
  templateUrl: './dialog-event.component.html',
  styleUrls: ['./dialog-event.component.css']
})
export class DialogEventComponent implements OnInit {


  productForm !: FormGroup;
  actionBtn: string="save"
  constructor(private fromBuilder : FormBuilder,
    private api: ApiService ,
    @Inject(MAT_DIALOG_DATA) public editData : any,
    private dialogRef :MatDialogRef<DialogEventComponent> ) { }

  ngOnInit(): void {
    this.productForm=this.fromBuilder.group({

      eventTitle :['',Validators.required],
      eventDescription :['',Validators.required],
      startDate :['',Validators.required],
      endDate :['',Validators.required],
      avenue:['',Validators.required],
      maxMembers :['',Validators.required],

    })
    if(this.editData){
      this.actionBtn="Update";

      this.productForm.controls['eventTitle' ].setValue(this.editData.eventTitle);

      this.productForm.controls['eventDescription'].setValue(this.editData.eventDescription);
      this.productForm.controls['startDate']. setValue(this.editData.startDate);
      this.productForm.controls['endDate'].setValue(this.editData.endDate);
      this.productForm.controls['avenue'].setValue(this.editData.avenue);
      this.productForm.controls['maxMembers'].setValue(this.editData.maxMembers);

     }

  }
  addProduct(){
    console.log(this.productForm.value);
    if(!this.editData){
      if(this.productForm.valid){
        this.api.postProduct(this.productForm.value)
        .subscribe({
          next:(res)=>{
            alert("Product added successfully")
            this.productForm.reset();
            this.dialogRef.close('save');
            window.location.reload();
          },
          error:()=>{
            alert("Error while adding the product")
          }
        })
      }

    }else {
      this.updateProduct()
    }
  }
  updateProduct(){
    this.api.putProduct( this.editData.eventId,this.productForm.value)
     .subscribe({
       next:(res)=>{
         alert("Product updated Successfully");
         this.productForm.reset();
         this.dialogRef.close('update');
         window.location.reload();
       },
       error:()=>{
         alert("Error while updating the record!!");
       }
     })
   }

}
