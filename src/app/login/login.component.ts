import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SignInData } from '../Modal/signInData';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isFormInvalid =false;
  areCredentialsInvalid =false;
  check="wrap-input100 validate-input m-b-16 ";

  constructor(private authenticationService:ApiService) { }

  ngOnInit(): void {
  }
  ngSubmit(signInForm:NgForm){
    if (!signInForm.valid){
      this.isFormInvalid =true;
      this.areCredentialsInvalid=false;
      return;
    }
    this.checkCredentials(signInForm);


  }
  private checkCredentials(signInForm: NgForm){

    const signInData =new SignInData(signInForm.value.email, signInForm.value.password);
    if(!this.authenticationService.authenticate(signInData)){
      this.isFormInvalid = false;
     if( this.areCredentialsInvalid = true ){
      this.check="wrap-input100 validate-input m-b-16 alert-validate";
     };



    }
  }

}
