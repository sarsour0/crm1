import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table'
import { DialogProspectComponent } from '../dialog-prospect/dialog-prospect.component';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-prospect',
  templateUrl: './prospect.component.html',
  styleUrls: ['./prospect.component.css']
})
export class ProspectComponent implements OnInit {

  
  displayedColumns: string[] = ['prospectId','nom', 'prenom','email' ,'cin','action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!:MatPaginator;
  @ViewChild(MatSort) sort!:MatSort;

constructor(private dialog : MatDialog,private api :ApiService){


}
darkmode=false;
  modetoggle(){
   this.darkmode =!this.darkmode;
   document.documentElement.setAttribute('data-theme',this.darkmode ? "dark" : "light");
 }
ngOnInit(): void {
    this.api.getClient().subscribe((res)=>{
    this.dataSource= new MatTableDataSource(res);
    this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      });
}
openDialog() {
  this.dialog.open(DialogProspectComponent , {
   width:'30%'
  });
}

getProspect(){
  this.api.getProspect()
  .subscribe({
    next:(res)=>{

     console.log( this.dataSource= new MatTableDataSource(res["http://localhost:8080/api/events"]));
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    },
    error:(err)=>{

      alert("Error while fetching the Records !!")
    }

  })
}

editProduct(row : any){

  this.dialog.open(DialogProspectComponent,{
    width :'30%',
    data:row
  }).afterClosed().subscribe(val=>{
    if(val==='update'){
      this.getProspect();
    }
  })


}
deleteProspect(prospectId:number){
  this.api.deleteprospect(prospectId)
  .subscribe({
    next:(res:any)=>{
      alert("Product Deleted Successfully");
      this.getProspect();
      window.location.reload();
    },
    error:()=>{
      alert("Error while deleting the product!! ")
    }
  })
}
applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}
}
