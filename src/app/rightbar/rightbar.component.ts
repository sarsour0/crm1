import { Component, OnInit, Output,EventEmitter } from '@angular/core';


@Component({
  selector: 'app-rightbar',
  templateUrl: './rightbar.component.html',
  styleUrls: ['./rightbar.component.css']
})
export class RightbarComponent implements OnInit {


  @Output() mode = new EventEmitter<boolean>();



  constructor() { }

  ngOnInit(): void {

  }
  darkmode=false;
  modetoggle(){

   this.darkmode =!this.darkmode;
   document.documentElement.setAttribute('data-theme',this.darkmode ? "dark" : "light");
 }



}
