import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http : HttpClient) { }
  postProduct(data :any){
    return this.http.post<any>("http://localhost:8080/api/event",data);
  }
  getProduct():Observable<any>{
    return this.http.get("http://localhost:8080/api/events");
  }
  putProduct(eventId: number, data:any){
    console.log(eventId)
    return this.http.put<any>("http://localhost:8080/api/event/"+eventId ,data);
  }
  deleteProduct(eventId:number){
    return this.http.delete<any>("http://localhost:8080/api/event/"+eventId);
}
 getClient():Observable<any>{
    return this.http.get("http://localhost:8080/api/clients");
  }
  deleteClient(clientId:number){
    return this.http.delete<any>("http://localhost:8080/api/client/"+clientId);
  }
  putClient(clientId: number, data:any){
    console.log(clientId)
    return this.http.put<any>("http://localhost:8080/api/client/"+clientId ,data);
  }
  getProspect():Observable<any>{
    return this.http.get("http://localhost:8080/api/prospect");
  }
  deleteprospect(prospectId:number){
    return this.http.delete<any>("http://localhost:8080/api/prospect/"+prospectId);
  }
  putprospect(prospectId: number, data:any){
    console.log(prospectId)
    return this.http.put<any>("http://localhost:8080/api/prospect/"+prospectId ,data);
  }


}
