import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupEventComponent } from './sup-event.component';

describe('SupEventComponent', () => {
  let component: SupEventComponent;
  let fixture: ComponentFixture<SupEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
