import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-sup-event',
  templateUrl: './sup-event.component.html',
  styleUrls: ['./sup-event.component.css']
})
export class SupEventComponent implements OnInit {

  constructor(private api:ApiService,private dialogRef :MatDialogRef<SupEventComponent>,
    @Inject(MAT_DIALOG_DATA) public editData : any) { }

  ngOnInit(): void {
  }
  deleteProduct(eventId:number){
    this.api.deleteProduct(eventId)
    .subscribe({
      next:(res)=>{
        this.dialogRef.close();
        window.location.reload();

      },
      error:()=>{

      }
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
